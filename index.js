var express = require('express');
const bodyParser = require('body-parser');
var path = require('path');
var session = require('express-session');
var cors = require('cors');
var routes = require('./routes')
var app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors())
app.use(session({
  key: 'user_sid',
  secret: 'somerandonstuffs',
  resave: false,
  saveUninitialized: false,
  cookie: {
      expires: 600000
  }
}));

app.use('/api', routes)

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname + '/login.html'))
})

var server = app.listen(8081, function() {}); 