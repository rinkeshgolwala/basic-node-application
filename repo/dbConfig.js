var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost:27017';

var dbConnect = (callback) => {
  MongoClient.connect(url, function(err, client) {
    if (err) {
      callback(error, null)
    } else {
      callback(null, client)
    }
  })
}

module.exports = dbConnect