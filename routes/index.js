var express = require('express');
var authorize = require('../middleware/authorize');
var editorsApi = require('../api/editorsApi')
var loginApi = require('../api/loginApi')
var router = express.Router();

router.get('/editors', authorize, editorsApi.getAllEditors);
router.get('/editor/:id', authorize, editorsApi.getEditorById);
router.post('/login', loginApi.validateUser);

module.exports = router;