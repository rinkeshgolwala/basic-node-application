1.  clone this project
2.  run 'npm install' command
3.  run 'npm start'
4.  The server will start on 8081 port.
5.  APIs: /api/login/, (Get)
          /api/editors/, (Get)
          /api/editir/:id (Post)

**** Mongodb database setup ***
1. dbScripts folder has 2 JSON collections.
2. Create 'TechifyDB' Database in mongo.
3. Create 'editors' and 'user_details' collections in mongo.
4. Users: username: Admin1, password: IAmAdmin1, role: admin,
          username: Editor1, password: IAmEditor1, role: editor,
          username: Editor2, password: IAmEditor2, role: editor,