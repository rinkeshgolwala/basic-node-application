var dbConnect = require('../repo/dbConfig')

var editorsApi = {
  getAllEditors: function (req, res) {
    if(req.session.user.role === 'admin') {
      dbConnect(function(err, client) {
        if (err) {
          res.status(500).send('DB connection failed')
        } else {
          const db = client.db('TechifyDB');
          var collection = db.collection('editors');
          collection.find({}).toArray(function(err, editors) {
            res.send(editors)
            client.close()
          });
        }
      });
    } else {
      res.status(403).send(`You don't have permission`)
    }
  },

  getEditorById: function (req, res) {
    let id=Number(req.params.id)
    dbConnect(function(err, client) {
      const db = client.db('TechifyDB');
      var collection = db.collection('editors');
      collection.findOne({id: id}, function(err, editors) {
        if (err) {
          res.status(500).send('DB connection failed')
        } else {
          res.send(editors)
          client.close()
        }
      });
    });
  }
}

module.exports = editorsApi