const bcrypt = require('bcrypt');
var dbConnect = require('../repo/dbConfig')

var loginApi = {
  validateUser: function (req, res) {
    let {username, password} = req.body
    if (!username || !password) {
      res.status(401).send("Username/Password can't be empty")
    } else {
      dbConnect(function(err, client) {
        if (err) {
          res.status(500).send('DB connection failed')
        } else {
          const db = client.db('TechifyDB');
          var collection = db.collection('user_details');
          collection.findOne({user_name: username.toLowerCase()}, function(err, user) {
            bcrypt.compare(password, user.password, function(err, result) {
              if (result) {
                req.session.user = {...user, password: ''};
                // res.status(200).send("success")
                user.role === 'admin' ? res.redirect('/api/editors') : res.redirect(`/api/editor/${user.id}`)
              } else {
                res.status(401).send('Incorrect username or password')
              }
            })
            client.close()
          });
        }
      });
    }
  }
}

module.exports = loginApi